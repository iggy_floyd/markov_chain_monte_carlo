# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


description = 'Application Monte-Carlo techniques in Option Trading'



all: configuration doc


# to check that the system has all needed components
configuration: configure
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac


doc: README.rst
	-@mkdir doc 2>/dev/null
	-@rst-tool/create_docs.sh README.rst `basename $(PWD)` $(description); mv README.html doc;

test:
	-@echo "Test: Not implemented"


run: configuration test
	-@echo "Run: Not implemented"


build: configuration test
	-@echo "Build: Not implemented"


# to clean all temporary stuff
clean: 
	-@rm -r config.log autom4te.cache
	-@rm -r doc/



.PHONY: configuration clean all doc run test build

